/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.common;

import java.io.Serializable;
import java.util.Collection;

/**
 * A WorkRecordRequest is a transfer object for the web service layer reflecting
 * to the business domain object WorkRecord.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public class WorkRecordRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String date;
    private String type;
    private Collection<WorkingSlotWSTO> slots;
    private Collection<WorkReportWSTO> reports;

    /**
     * Create a new WorkRecordRequest.
     */
    public WorkRecordRequest() {
        super();
    }

    /**
     * Get the id.
     * 
     * @return the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the id.
     * 
     * @param id
     *            The id to set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get the date.
     * 
     * @return the date.
     */
    public String getDate() {
        return date;
    }

    /**
     * Set the date.
     * 
     * @param date
     *            The date to set.
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Get the type.
     * 
     * @return the type.
     */
    public String getType() {
        return type;
    }

    /**
     * Set the type.
     * 
     * @param type
     *            The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get the slots.
     * 
     * @return the slots.
     */
    public Collection<WorkingSlotWSTO> getSlots() {
        return slots;
    }

    /**
     * Set the slots.
     * 
     * @param slots
     *            The slots to set.
     */
    public void setSlots(Collection<WorkingSlotWSTO> slots) {
        this.slots = slots;
    }

    /**
     * Get the reports.
     * 
     * @return the reports.
     */
    public Collection<WorkReportWSTO> getReports() {
        return reports;
    }

    /**
     * Set the reports.
     * 
     * @param reports
     *            The reports to set.
     */
    public void setReports(Collection<WorkReportWSTO> reports) {
        this.reports = reports;
    }

    /**
     * Check whether this instance has a valid state.
     * 
     * @return <code>true</code> if valid, otherwise <code>false</code>
     */
    public boolean isValid() {
        if (getDate() == null) {
            return false;
        }
        return true;
    }

    /**
     * Use date, reports, slots and type.
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((reports == null) ? 0 : reports.hashCode());
        result = prime * result + ((slots == null) ? 0 : slots.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    /**
     * Use date, reports, slots and type.
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        WorkRecordRequest other = (WorkRecordRequest) obj;
        if (date == null) {
            if (other.date != null) {
                return false;
            }
        } else if (!date.equals(other.date)) {
            return false;
        }
        if (reports == null) {
            if (other.reports != null) {
                return false;
            }
        } else if (!reports.equals(other.reports)) {
            return false;
        }
        if (slots == null) {
            if (other.slots != null) {
                return false;
            }
        } else if (!slots.equals(other.slots)) {
            return false;
        }
        if (type == null) {
            if (other.type != null) {
                return false;
            }
        } else if (!type.equals(other.type)) {
            return false;
        }
        return true;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("WorkRecordRequest [id=");
        builder.append(id);
        builder.append(", date=");
        builder.append(date);
        builder.append(", type=");
        builder.append(type);
        builder.append(", slots=");
        builder.append(slots);
        builder.append(", reports=");
        builder.append(reports);
        builder.append("]");
        return builder.toString();
    }
}
