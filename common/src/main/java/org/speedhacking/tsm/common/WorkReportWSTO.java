/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.common;

import java.io.Serializable;

/**
 * A WorkReportWSTO is a transfer object for the web service layer reflecting to
 * the business domain object WorkReport.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public class WorkReportWSTO implements Serializable {

    private static final long serialVersionUID = 1464043698486766180L;
    private Long id;
    private String comment;
    private String projectCode;

    /**
     * Create a new WorkReportWSTO.
     */
    public WorkReportWSTO() {
        super();
    }

    /**
     * Get the id.
     * 
     * @return the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the id.
     * 
     * @param id
     *            The id to set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get the comment.
     * 
     * @return the comment.
     */
    public String getComment() {
        return comment;
    }

    /**
     * Set the comment.
     * 
     * @param comment
     *            The comment to set.
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Get the projectCode.
     * 
     * @return the projectCode.
     */
    public String getProjectCode() {
        return projectCode;
    }

    /**
     * Set the projectCode.
     * 
     * @param projectCode
     *            The projectCode to set.
     */
    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    /**
     * Use arrived and left.
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((projectCode == null) ? 0 : projectCode.hashCode());
        return result;
    }

    /**
     * Use arrived and left.
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        WorkReportWSTO other = (WorkReportWSTO) obj;
        if (comment == null) {
            if (other.comment != null) {
                return false;
            }
        } else if (!comment.equals(other.comment)) {
            return false;
        }
        if (projectCode == null) {
            if (other.projectCode != null) {
                return false;
            }
        } else if (!projectCode.equals(other.projectCode)) {
            return false;
        }
        return true;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("WorkReportWSTO [id=");
        builder.append(id);
        builder.append(", comment=");
        builder.append(comment);
        builder.append(", projectCode=");
        builder.append(projectCode);
        builder.append("]");
        return builder.toString();
    }
}
