/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * A WorkRecordResponse is a transfer object for the web service layer that
 * encapsulates a Collection of {@link WorkRecord}s along with additional
 * response data.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 * @see org.speedhacking.tsm.backend.domain.WorkRecord
 */
public class WorkRecordResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    private Collection<WorkRecordWSTO> saved = new ArrayList<WorkRecordWSTO>();
    private int error;

    /**
     * Create a new WorkRecordResponse.
     * 
     * @param saved
     *            A Collection of stored or modified {@link WorkRecord}s
     * @param error
     *            Some error code
     */
    public WorkRecordResponse(Collection<WorkRecordWSTO> saved, int error) {
        super();
        this.saved = saved;
        this.error = error;
    }

    /**
     * Create a new WorkRecordResponse.
     * 
     * @param saved
     *            A Collection of stored or modified {@link WorkRecord}s
     */
    public WorkRecordResponse(Collection<WorkRecordWSTO> saved) {
        super();
        this.saved = saved;
    }

    /**
     * Create a new WorkRecordResponse.
     */
    public WorkRecordResponse() {
        super();
    }

    /**
     * Get the saved.
     * 
     * @return the saved.
     */
    public Collection<WorkRecordWSTO> getSaved() {
        return saved;
    }

    /**
     * Set the saved.
     * 
     * @param saved
     *            The saved to set.
     */
    public void setSaved(Collection<WorkRecordWSTO> saved) {
        this.saved = saved;
    }

    /**
     * Add a new {@link WorkRecordWSTO} to the Collection of
     * {@link WorkRecordWSTO}s
     * 
     * @param workRecord
     *            The {@link WorkRecordWSTO} to be added
     * @return <code>true</code> if this collection changed as a result of the
     *         call
     */
    public boolean addWorkRecord(WorkRecordWSTO workRecord) {
        return this.saved.add(workRecord);
    }

    /**
     * Get the error.
     * 
     * @return the error.
     */
    public int getError() {
        return error;
    }

    /**
     * Set the error.
     * 
     * @param error
     *            The error to set.
     */
    public void setError(int error) {
        this.error = error;
    }

    /**
     * Use error and saved.
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + error;
        result = prime * result + ((saved == null) ? 0 : saved.hashCode());
        return result;
    }

    /**
     * Use error and saved.
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        WorkRecordResponse other = (WorkRecordResponse) obj;
        if (error != other.error) {
            return false;
        }
        if (saved == null) {
            if (other.saved != null) {
                return false;
            }
        } else if (!saved.equals(other.saved)) {
            return false;
        }
        return true;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("WorkRecordResponse [saved=");
        builder.append(saved);
        builder.append(", error=");
        builder.append(error);
        builder.append("]");
        return builder.toString();
    }
}
