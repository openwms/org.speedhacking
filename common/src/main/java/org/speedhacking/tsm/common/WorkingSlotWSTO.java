/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.common;

import java.io.Serializable;

/**
 * A WorkingSlotWSTO is a transfer object for the web service layer reflecting
 * to the business domain object WorkingSlot.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public class WorkingSlotWSTO implements Serializable {

    private static final long serialVersionUID = 8289340292951546447L;
    private Long id;
    private String arrived;
    private String left;

    /**
     * Create a new WorkingSlotWSTO.
     */
    public WorkingSlotWSTO() {
        super();
    }

    /**
     * Create a new WorkingSlotWSTO.
     * 
     * @param arrived
     * @param left
     */
    public WorkingSlotWSTO(String arrived, String left) {
        super();
        this.arrived = arrived;
        this.left = left;
    }

    /**
     * Get the id.
     * 
     * @return the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the id.
     * 
     * @param id
     *            The id to set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get the arrived.
     * 
     * @return the arrived.
     */
    public String getArrived() {
        return arrived;
    }

    /**
     * Set the arrived.
     * 
     * @param arrived
     *            The arrived to set.
     */
    public void setArrived(String arrived) {
        this.arrived = arrived;
    }

    /**
     * Get the left.
     * 
     * @return the left.
     */
    public String getLeft() {
        return left;
    }

    /**
     * Set the left.
     * 
     * @param left
     *            The left to set.
     */
    public void setLeft(String left) {
        this.left = left;
    }

    /**
     * Use arrived and left.
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((arrived == null) ? 0 : arrived.hashCode());
        result = prime * result + ((left == null) ? 0 : left.hashCode());
        return result;
    }

    /**
     * Use arrived and left.
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        WorkingSlotWSTO other = (WorkingSlotWSTO) obj;
        if (arrived == null) {
            if (other.arrived != null) {
                return false;
            }
        } else if (!arrived.equals(other.arrived)) {
            return false;
        }
        if (left == null) {
            if (other.left != null) {
                return false;
            }
        } else if (!left.equals(other.left)) {
            return false;
        }
        return true;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("WorkingSlotWSTO [id=");
        builder.append(id);
        builder.append(", arrived=");
        builder.append(arrived);
        builder.append(", left=");
        builder.append(left);
        builder.append("]");
        return builder.toString();
    }
}
