/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.common;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.speedhacking.tsm.common.domain.WorkRecordType;

/**
 * A WorkRecordWSTO.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public class WorkRecordWSTO implements Serializable {

    private static final long serialVersionUID = 6342309420520228030L;
    private Long id;
    private String username;
    private String date;
    private String overtime;
    private String overtimeCurrentWeek;
    private String overtimeCurrentMonth;
    private WorkRecordType type;
    private Set<WorkingSlotWSTO> slots = new HashSet<WorkingSlotWSTO>();
    private Set<WorkReportWSTO> reports = new HashSet<WorkReportWSTO>();

    public static class Builder {

        private WorkRecordWSTO wr;

        /**
         * Create a new WorkRecordWSTO.Builder.
         */
        public Builder(String username, String date) {
            wr = new WorkRecordWSTO(username, date);
        }

        public Builder withId(Long id) {
            wr.setId(id);
            return this;
        }

        public Builder withOvertime(String overtime) {
            wr.setOvertime(overtime);
            return this;
        }

        public Builder withOvertimeCurrentWeek(String overtimeCurrentWeek) {
            wr.setOvertimeCurrentWeek(overtimeCurrentWeek);
            return this;
        }

        public Builder withOvertimeCurrentMonth(String overtimeCurrentMonth) {
            wr.setOvertimeCurrentMonth(overtimeCurrentMonth);
            return this;
        }

        public Builder withWorkRecordType(WorkRecordType workRecordType) {
            wr.setType(workRecordType);
            return this;
        }

        public Builder withSlots(Set<WorkingSlotWSTO> slots) {
            wr.setSlots(slots);
            return this;
        }

        public Builder withReports(Set<WorkReportWSTO> reports) {
            wr.setReports(reports);
            return this;
        }

        public WorkRecordWSTO build() {
            return wr;
        }
    }

    /**
     * Create a new WorkRecordWSTO.
     */
    public WorkRecordWSTO() {
        super();
    }

    /**
     * Create a new WorkRecordWSTO.
     * 
     * @param username
     * @param date
     */
    public WorkRecordWSTO(String username, String date) {
        super();
        this.username = username;
        this.date = date;
    }

    /**
     * Get the id.
     * 
     * @return the id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Get the type.
     * 
     * @return the type.
     */
    public WorkRecordType getType() {
        return type;
    }

    /**
     * Set the type.
     * 
     * @param type
     *            The type to set.
     */
    public void setType(WorkRecordType type) {
        this.type = type;
    }

    /**
     * Get the slots.
     * 
     * @return the slots.
     */
    public Set<WorkingSlotWSTO> getSlots() {
        return slots;
    }

    /**
     * Set the slots.
     * 
     * @param slots
     *            The slots to set.
     */
    public void setSlots(Set<WorkingSlotWSTO> slots) {
        this.slots = slots;
    }

    /**
     * Add a new Slot.
     * 
     * @param slot
     *            The new Slot to add
     * @return see {@link Collection#add(Object)}
     */
    public boolean addSlot(WorkingSlotWSTO slot) {
        return this.slots.add(slot);
    }

    /**
     * Remove a Slot.
     * 
     * @param slot
     *            The Slot to remove
     */
    public void remove(WorkingSlotWSTO slot) {
        slots.remove(slot);
    }

    /**
     * Get the reports.
     * 
     * @return the reports.
     */
    public Set<WorkReportWSTO> getReports() {
        return reports;
    }

    /**
     * Set the reports.
     * 
     * @param reports
     *            The reports to set.
     */
    public void setReports(Set<WorkReportWSTO> reports) {
        this.reports = reports;
    }

    /**
     * Get the username.
     * 
     * @return the username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Get the date.
     * 
     * @return the date.
     */
    public String getDate() {
        return date;
    }

    /**
     * Get the overtime.
     * 
     * @return the overtime.
     */
    public String getOvertime() {
        return overtime;
    }

    /**
     * Get the overtimeCurrentWeek.
     * 
     * @return the overtimeCurrentWeek.
     */
    public String getOvertimeCurrentWeek() {
        return overtimeCurrentWeek;
    }

    /**
     * Get the overtimeCurrentMonth.
     * 
     * @return the overtimeCurrentMonth.
     */
    public String getOvertimeCurrentMonth() {
        return overtimeCurrentMonth;
    }

    /**
     * Set the id.
     * 
     * @param id
     *            The id to set.
     */
    void setId(Long id) {
        this.id = id;
    }

    /**
     * Set the overtime.
     * 
     * @param overtime
     *            The overtime to set.
     */
    void setOvertime(String overtime) {
        this.overtime = overtime;
    }

    /**
     * Set the overtimeCurrentWeek.
     * 
     * @param overtimeCurrentWeek
     *            The overtimeCurrentWeek to set.
     */
    void setOvertimeCurrentWeek(String overtimeCurrentWeek) {
        this.overtimeCurrentWeek = overtimeCurrentWeek;
    }

    /**
     * Set the overtimeCurrentMonth.
     * 
     * @param overtimeCurrentMonth
     *            The overtimeCurrentMonth to set.
     */
    void setOvertimeCurrentMonth(String overtimeCurrentMonth) {
        this.overtimeCurrentMonth = overtimeCurrentMonth;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder2 = new StringBuilder();
        builder2.append("WorkRecordWSTO [id=");
        builder2.append(id);
        builder2.append(", username=");
        builder2.append(username);
        builder2.append(", date=");
        builder2.append(date);
        builder2.append(", overtime=");
        builder2.append(overtime);
        builder2.append(", overtimeCurrentWeek=");
        builder2.append(overtimeCurrentWeek);
        builder2.append(", overtimeCurrentMonth=");
        builder2.append(overtimeCurrentMonth);
        builder2.append(", type=");
        builder2.append(type);
        builder2.append(", slots=");
        builder2.append(slots);
        builder2.append(", reports=");
        builder2.append(reports);
        builder2.append("]");
        return builder2.toString();
    }
}
