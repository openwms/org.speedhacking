/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.backend.service;

import org.speedhacking.tsm.common.domain.User;

/**
 * An UserService used to find and update Users.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public interface UserService {

    /**
     * Find and return an User by its {@code userName}.
     * 
     * @param userName
     *            The name of the User
     * @return The User instance or <code>null</code> if no User with
     *         {@code userName} was found
     */
    User findByName(String userName);

    /**
     * Save an User instance to the persisted storage.
     * 
     * @param user
     *            The User instance to save
     * @return The saved User instance
     */
    User save(User user);
}
