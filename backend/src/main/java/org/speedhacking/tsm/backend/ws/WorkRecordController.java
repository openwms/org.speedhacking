/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.backend.ws;

import org.speedhacking.tsm.backend.ws.service.WorkRecordService;
import org.speedhacking.tsm.common.WorkRecordRequest;
import org.speedhacking.tsm.common.WorkRecordResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A WorkRecordController is a simple Spring managed web controller to managed
 * WorkRecords. Methods are exposed via RequestBindings and accept as well as
 * return JSON serialized objects. Furthermore this class just delegates to a
 * Spring managed transactional service that spans a transaction.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
@Controller
@RequestMapping("/workRecords")
public class WorkRecordController {

    @Autowired
    private WorkRecordService service;
    @Autowired
    private UserHolder userHolder;

    @RequestMapping(value = "/{year}/{month}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, headers = "Content-Type=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public WorkRecordResponse getWorkRecords(@PathVariable("year") String year, @PathVariable("month") String month) {
        return service.getWorkRecords(userHolder.getAuthenticatedUser(), year, month);
    }

    /**
     * Save an already existing WorkRecord wrapped by an transfer object of type
     * {@link WorkRecordRequest}. This method is mapped to incoming PUT requests
     * in JSON format.
     * 
     * @param workRecordRequest
     *            The wrapped WorkRecord
     * @return An response object containing some status code and data
     */
    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, headers = "Content-Type=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public WorkRecordResponse saveWorkRecord(@RequestBody WorkRecordRequest workRecordRequest) {
        return service.saveWorkRecord(userHolder.getAuthenticatedUser(), workRecordRequest);
    }

    /**
     * Create a new WorkRecord wrapped by an transfer object of type
     * {@link WorkRecordRequest}. This method is mapped to incoming POST
     * requests in JSON format.
     * 
     * @param workRecordRequest
     *            The wrapped WorkRecord
     * @return An response object containing some status code and data
     */
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, headers = "Content-Type=application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public WorkRecordResponse createWorkRecord(@RequestBody WorkRecordRequest workRecordRequest) {
        return service.createWorkRecord(userHolder.getAuthenticatedUser(), workRecordRequest);
    }
}
