/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.backend.service;

/**
 * A BackendServiceException.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public class BackendServiceException extends Exception {

    private static final long serialVersionUID = 1L;
    private int errorCode = 0;

    /**
     * Create a new BackendServiceException.
     */
    public BackendServiceException() {}

    /**
     * Create a new BackendServiceException.
     * 
     * @param message
     */
    public BackendServiceException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * Create a new BackendServiceException.
     * 
     * @param cause
     */
    public BackendServiceException(Throwable cause, int errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    /**
     * Create a new BackendServiceException.
     * 
     * @param message
     * @param cause
     */
    public BackendServiceException(String message, Throwable cause, int errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    /**
     * Get the errorCode.
     * 
     * @return the errorCode.
     */
    public int getErrorCode() {
        return errorCode;
    }
}
