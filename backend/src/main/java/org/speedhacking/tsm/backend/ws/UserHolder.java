/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.backend.ws;

import org.speedhacking.tsm.backend.service.UserService;
import org.speedhacking.tsm.common.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * A UserHolder is a temporary implementation to provide an instance of an User.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
@Controller
public class UserHolder {

    @Autowired
    private UserService service;
    private ThreadLocal<User> user = new ThreadLocal<User>();

    /**
     * Create a new UserHolder.
     */
    private UserHolder() {}

    /**
     * Dummy!!!
     * 
     * @return The User
     */
    public User getAuthenticatedUser() {
        if (user.get() == null) {
            user.set(service.findByName("heiko"));
        }
        return user.get();
    }
}
