/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.backend.ws.service.impl;

import java.util.Calendar;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.speedhacking.tsm.backend.service.BackendServiceException;
import org.speedhacking.tsm.backend.service.TimeReportingService;
import org.speedhacking.tsm.backend.ws.service.WorkRecordService;
import org.speedhacking.tsm.common.WorkRecordConverter;
import org.speedhacking.tsm.common.WorkRecordRequest;
import org.speedhacking.tsm.common.WorkRecordResponse;
import org.speedhacking.tsm.common.domain.User;
import org.speedhacking.tsm.common.domain.WorkRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A WorkRecordServiceImpl is the Spring managed transactional service
 * implementation of {@link WorkRecordService}.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
@Transactional
@Service
public class WorkRecordServiceImpl implements WorkRecordService {

    private static final Logger LOGGER = Logger.getLogger(WorkRecordServiceImpl.class);
    @Autowired
    private TimeReportingService timesService;

    /**
     * {@inheritDoc}
     * 
     * GET
     * 
     * @see org.speedhacking.tsm.backend.ws.service.WorkRecordService#getWorkRecords(org.speedhacking.tsm.common.domain.User,
     *      java.lang.String, java.lang.String)
     */
    @Transactional(readOnly = true)
    @Override
    public WorkRecordResponse getWorkRecords(User user, String year, String month) {
        WorkRecordResponse response = new WorkRecordResponse();
        if (year == null || month == null) {
            response.setError(HttpStatus.BAD_REQUEST.value());
            return response;
        }

        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        try {
            startDate.set(Integer.parseInt(year), getMonth(month), 1);
            endDate.set(Integer.parseInt(year), getMonth(month), startDate.getActualMaximum(Calendar.DAY_OF_MONTH));
            Collection<WorkRecord> saved = timesService.getWorkRecords(user, startDate, endDate);
            for (WorkRecord wr : saved) {
                response.addWorkRecord(WorkRecordConverter.createFrom(wr));
            }
            response.setError(HttpStatus.OK.value());
        } catch (IllegalArgumentException iae) {
            response.setError(HttpStatus.BAD_REQUEST.value());
        } catch (Exception ex) {
            response.setError(HttpStatus.SERVICE_UNAVAILABLE.value());
        }
        return response;
    }

    /**
     * {@inheritDoc}
     * 
     * PUT
     * 
     * @see org.speedhacking.tsm.backend.ws.service.WorkRecordService#saveWorkRecord(org.speedhacking.tsm.common.domain.User,
     *      org.speedhacking.tsm.common.WorkRecordRequest)
     */
    @Override
    public WorkRecordResponse saveWorkRecord(User user, WorkRecordRequest workRecordRequest) {
        WorkRecordResponse response = new WorkRecordResponse();
        if (workRecordRequest == null) {
            response.setError(HttpStatus.NOT_FOUND.value());
            return response;
        }

        // If id is null create a new entry
        if (workRecordRequest.getId() == null) {
            response.setError(HttpStatus.BAD_REQUEST.value());
            return response;
        }

        // If no record exists, create an error response
        WorkRecord saved = timesService.getById(workRecordRequest.getId());
        if (saved == null) {
            response.setError(HttpStatus.NOT_FOUND.value());
            return response;
        }

        WorkRecord wr = WorkRecordConverter.copyTo(workRecordRequest, saved);
        try {
            WorkRecord savedOne = timesService.saveWorkRecord(user, wr);
            response.addWorkRecord(WorkRecordConverter.createFrom(savedOne));
            response.setError(HttpStatus.OK.value());
        } catch (BackendServiceException e) {
            response.addWorkRecord(WorkRecordConverter.createFrom(wr));
            response.setError(HttpStatus.CONFLICT.value());
        }
        return response;
    }

    /**
     * {@inheritDoc}
     * 
     * POST
     * 
     * @see org.speedhacking.tsm.backend.ws.service.WorkRecordService#createWorkRecord(org.speedhacking.tsm.common.domain.User,
     *      org.speedhacking.tsm.common.WorkRecordRequest)
     */
    @Override
    public WorkRecordResponse createWorkRecord(User user, WorkRecordRequest workRecordRequest) {
        WorkRecordResponse response = new WorkRecordResponse();
        if (workRecordRequest == null || !workRecordRequest.isValid()) {
            LOGGER.error("Cannot create a new WorkRecord, because the input parameter or the date of the record to create is null. ["
                    + workRecordRequest + "]");
            response.setError(HttpStatus.NOT_FOUND.value());
            return response;
        }
        DateTime currentDate = WorkRecordConverter.convertDate(workRecordRequest.getDate());
        WorkRecord wr = WorkRecordConverter.copyTo(workRecordRequest, new WorkRecord(user, currentDate.toDate()));

        try {
            WorkRecord savedOne = timesService.saveWorkRecord(user, wr);
            response.addWorkRecord(WorkRecordConverter.createFrom(savedOne));
            response.setError(HttpStatus.OK.value());
        } catch (BackendServiceException e) {
            response.addWorkRecord(WorkRecordConverter.createFrom(wr));
            response.setError(HttpStatus.CONFLICT.value());
        }
        return response;
    }

    private int getMonth(String m) {
        int month = Integer.parseInt(m) - 1;
        if (month < 0 || month > 11) {
            LOGGER.error("Month " + m + " not in convertable range!");
            throw new IllegalArgumentException();
        }
        return month;
    }
}
