/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.backend.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;

import org.speedhacking.tsm.backend.service.UserService;
import org.speedhacking.tsm.common.domain.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * An UserServiceImpl is a Spring managed transactional service implementation.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
@Transactional
@Service
public class UserServiceImpl implements UserService {

    @PersistenceContext
    private EntityManager em;

    /**
     * {@inheritDoc}
     * 
     * @see org.speedhacking.tsm.backend.service.UserService#findByName(java.lang.String)
     */
    @Override
    public User findByName(@NotNull String userName) {
        try {
            return em.createNamedQuery(User.NQ_BY_NAME, User.class).setParameter(User.NQ_PARAM_NAME, userName)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.speedhacking.tsm.backend.service.UserService#save(org.speedhacking.tsm.common.domain.User)
     */
    @Override
    public User save(@NotNull User user) {
        User persistedUser = em.find(User.class, user.getId());
        if (null == persistedUser) {
            em.persist(user);
        }
        return em.merge(user);
    }
}
