/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.backend.ws.service;

import org.speedhacking.tsm.common.WorkRecordRequest;
import org.speedhacking.tsm.common.WorkRecordResponse;
import org.speedhacking.tsm.common.domain.User;

/**
 * A WorkRecordService defines the functionality to do basic operations on
 * internally managed WorkRecords objects. This is an outer service facade to
 * web controllers.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public interface WorkRecordService {

    WorkRecordResponse getWorkRecords(User user, String year, String month);

    /**
     * Save existing WorkRecords encapsulated in the {@link WorkRecordRequest}
     * object.
     * 
     * @param user
     *            The {@link User} is the owner of the WorkRecord
     * @param workRecordRequest
     *            The request object
     * @return An response object
     */
    WorkRecordResponse saveWorkRecord(User user, WorkRecordRequest workRecordRequest);

    /**
     * Create a new WorkRecord for an User {@code user}.
     * 
     * @param user
     *            The {@link User} is the owner of the WorkRecord
     * @param workRecordRequest
     *            The request object
     * @return An response object
     */
    WorkRecordResponse createWorkRecord(User user, WorkRecordRequest workRecordRequest);
}
