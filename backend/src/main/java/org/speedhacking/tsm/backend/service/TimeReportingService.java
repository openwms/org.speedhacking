/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.backend.service;

import java.util.Calendar;
import java.util.Collection;

import javax.validation.constraints.NotNull;

import org.speedhacking.tsm.common.domain.User;
import org.speedhacking.tsm.common.domain.WorkRecord;

/**
 * A TimeReportingService acts as Repository (in early days as DAO) and deals
 * with {@link WorkRecord}s.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public interface TimeReportingService {

    /**
     * Find and return all {@link WorkRecord}s belonging to an {@link User}
     * within a defined range from {@code startDate} till {@code dueDate}. All
     * parameters must be set and not <code>null</code>
     * 
     * @param user
     *            The {@link User} to find the {@link WorkRecord}s for
     * @param startDate
     *            The date from where to search (included)
     * @param dueDate
     *            The date until the search happens (included)
     * @return All {@link WorkRecord}s found for the particular range for that
     *         {@code User}
     */
    Collection<WorkRecord> getWorkRecords(@NotNull User user, @NotNull Calendar startDate, @NotNull Calendar dueDate);

    /**
     * Find and return a {@link WorkRecord} by its ID
     * 
     * @param id
     *            The ID of the {@link WorkRecord}
     * @return The {@link WorkRecord} or <code>null</code> if not found
     */
    WorkRecord getById(long id);

    /**
     * Save an existing or create an {@link WorkRecord} instance and return the
     * saved instance.
     * 
     * @param user
     *            The {@link User} to save the record for
     * @param workRecord
     *            The instance that was saved or created
     */
    WorkRecord saveWorkRecord(@NotNull User user, WorkRecord workRecord) throws BackendServiceException;

    /**
     * Calculate the overtime for a day that is determined in the
     * {@code workRecord} instance.
     * 
     * @param workRecord
     *            The record to calculate the overtime for
     */
    void calculateOvertime(WorkRecord workRecord);

    /**
     * Calculate the overtime for a day that is determined in the
     * {@code workRecord} instance. Days are not recognized only the year and
     * month are used for calculation.
     * 
     * @param workRecord
     *            The record to calculate the overtime for
     */
    void calculateOvertimeMonth(WorkRecord workRecord);
}
