/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.backend.service.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;

import org.joda.time.Period;
import org.speedhacking.tsm.backend.service.BackendServiceException;
import org.speedhacking.tsm.backend.service.TimeReportingService;
import org.speedhacking.tsm.common.WorkRecordConverter;
import org.speedhacking.tsm.common.domain.User;
import org.speedhacking.tsm.common.domain.WorkRecord;
import org.speedhacking.tsm.common.domain.WorkingSlot;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * A TimeReportingServiceImpl.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
@Transactional(propagation = Propagation.MANDATORY)
@Repository
public class TimeReportingServiceImpl implements TimeReportingService {

    @PersistenceContext
    private EntityManager em;

    /**
     * @see org.speedhacking.tsm.backend.service.facade.TimeReportingService#getWorkRecords(org.speedhacking.tsm.common.domain.User,
     *      java.util.Calendar, java.util.Calendar)
     */
    @Override
    public Collection<WorkRecord> getWorkRecords(@NotNull User user, @NotNull Calendar startDate,
            @NotNull Calendar dueDate) {
        List<WorkRecord> resultList = em.createNamedQuery(WorkRecord.NQ_BY_USER_IN_RANGE, WorkRecord.class)
                .setParameter(WorkRecord.NQ_PARAM_USER, user)
                .setParameter(WorkRecord.NQ_PARAM_STARTDATE, startDate.getTime())
                .setParameter(WorkRecord.NQ_PARAM_ENDDATE, dueDate.getTime()).getResultList();
        return resultList == null ? Collections.<WorkRecord> emptyList() : resultList;
    }

    /**
     * @see org.speedhacking.tsm.backend.service.facade.TimeReportingService#saveWorkRecord(org.speedhacking.tsm.common.domain.WorkRecord)
     */
    @Override
    public WorkRecord saveWorkRecord(@NotNull User user, WorkRecord workRecord) throws BackendServiceException {
        WorkRecord existing;
        try {
            existing = em.createNamedQuery(WorkRecord.NQ_BY_USER_AND_DATE, WorkRecord.class)
                    .setParameter(WorkRecord.NQ_PARAM_USER, user)
                    .setParameter(WorkRecord.NQ_PARAM_DATE, workRecord.getDate()).getSingleResult();
        } catch (NoResultException nre) {
            em.persist(workRecord);
            existing = em.merge(workRecord);
        }
        existing.setSlots(workRecord.getSlots());
        existing.setReports(workRecord.getReports());
        existing.setType(workRecord.getType());
        calculateOvertime(existing);
        return em.merge(existing);
    }

    /**
     * @see org.speedhacking.tsm.backend.service.facade.TimeReportingService#getById(long)
     */
    @Override
    public WorkRecord getById(long id) {
        return em.find(WorkRecord.class, id);
    }

    /**
     * @see org.speedhacking.tsm.backend.service.facade.TimeReportingService#calculateOvertime(java.util.Date)
     */
    @Override
    public void calculateOvertime(WorkRecord workRecord) {
        Set<WorkingSlot> slots = workRecord.getSlots();
        Period p = null;
        for (WorkingSlot workingSlot : slots) {
            p = new Period(WorkRecordConverter.TIME_FORMATTER.parseMillis(workingSlot.getArrived()),
                    WorkRecordConverter.TIME_FORMATTER.parseMillis(workingSlot.getLeft())).plus(p);
        }
        workRecord.setOvertime(p);
    }

    /**
     * @see org.speedhacking.tsm.backend.service.facade.TimeReportingService#calculateOvertimeMonth(java.util.Date)
     */
    @Override
    public void calculateOvertimeMonth(WorkRecord workRecord) {
        // TODO [scherrer] Auto-generated method stub

    }
}
