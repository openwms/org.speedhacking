/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.common;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.speedhacking.tsm.common.domain.WorkRecord;
import org.speedhacking.tsm.common.domain.WorkRecordType;
import org.speedhacking.tsm.common.domain.WorkReport;
import org.speedhacking.tsm.common.domain.WorkingSlot;

/**
 * A WordRecordConverter.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public final class WorkRecordConverter {

    public static final PeriodFormatter PERIOD_FORMATTER = new PeriodFormatterBuilder().appendHours()
            .appendSeparator(":").minimumPrintedDigits(2).printZeroAlways().appendMinutes().toFormatter();
    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormat.forPattern("HH:mm");
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("YYYY-MM-DD");

    /**
     * Take over:
     * <ul>
     * <li>the type</li>
     * <li>all slots</li>
     * <li>and all reports</li>
     * </ul>
     * 
     * @param source
     *            The source object
     * @param target
     *            The target object
     * @return The modified target object
     */
    public static WorkRecord copyTo(WorkRecordRequest source, WorkRecord target) {
        WorkRecord changed = target;
        if (Arrays.asList(WorkRecordType.values()).contains(source.getType())) {
            changed.setType(WorkRecordType.valueOf(source.getType()));
        } else if (source.getType() == null || source.getType().isEmpty()) {
            changed.setType(null);
        }
        return transformChildren(source, changed);
    }

    public static WorkRecordWSTO createFrom(WorkRecord source) {
        return new WorkRecordWSTO.Builder(source.getUser().getName(), DATE_FORMATTER.print(source.getDate().getTime()))
                .withId(source.getId())
                .withOvertime(source.getOvertime() == null ? "" : source.getOvertime().toString(PERIOD_FORMATTER))
                .withOvertimeCurrentWeek(
                        source.getOvertimeCurrentWeek() == null ? "" : source.getOvertimeCurrentWeek().toString(
                                PERIOD_FORMATTER))
                .withOvertimeCurrentMonth(
                        source.getOvertimeCurrentMonth() == null ? "" : source.getOvertimeCurrentMonth().toString(
                                PERIOD_FORMATTER)).withWorkRecordType(source.getType())
                .withSlots(transform(source.getSlots())).build();
    }

    /**
     * Create business domain objects {@link WorkingSlot}s and
     * {@link WorkReport}s for each slot and report. Add those to the
     * {@link WorkRecord} {@code wr}.
     * 
     * @param source
     *            The source object
     * @param target
     *            The target object
     * @return The modified target object
     */
    public static WorkRecord transformChildren(WorkRecordRequest source, WorkRecord target) {
        if (null != source.getSlots()) {
            for (WorkingSlotWSTO slot : source.getSlots()) {
                target.addSlot(new WorkingSlot(slot.getId(), slot.getArrived(), slot.getLeft()));
            }
        }
        if (null != source.getReports()) {
            for (WorkReportWSTO workReport : source.getReports()) {
                target.addReport(new WorkReport(workReport.getId(), workReport.getComment(), workReport
                        .getProjectCode()));
            }
        }
        return target;
    }

    public static Set<WorkingSlotWSTO> transform(Set<WorkingSlot> source) {
        Set<WorkingSlotWSTO> result = new HashSet<WorkingSlotWSTO>(source.size());
        for (WorkingSlot workingSlot : source) {
            result.add(transform(workingSlot));
        }
        return result;
    }

    public static WorkingSlotWSTO transform(WorkingSlot source) {
        WorkingSlotWSTO result = new WorkingSlotWSTO();
        result.setArrived(source.getArrived());
        result.setLeft(source.getLeft());
        return result;
    }

    public static DateTime convertDate(String date) {
        return DATE_FORMATTER.parseDateTime(date);
    }
}
