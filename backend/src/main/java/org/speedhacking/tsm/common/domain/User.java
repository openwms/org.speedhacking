/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.common.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A User.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
@Entity
@NamedQueries({ @NamedQuery(name = User.NQ_BY_NAME, query = "select u from User u where u.name = :"
        + User.NQ_PARAM_NAME) })
@Table(name = "T_USER")
public class User extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Named Query to find an User by its {@code name}.
     * <ul>
     * <li>{@value #NQ_PARAM_NAME}: The Users name</li>
     * </ul>
     */
    public static final String NQ_BY_NAME = "User.findByName";
    /**
     * Parameter name {@code name}.
     */
    public static final String NQ_PARAM_NAME = "name";

    @NotNull
    // @Max(25)
    @Column(name = "C_NAME", unique = true, length = 25)
    private String name;
    @NotNull
    // @Max(25)
    @Column(name = "C_PASSWORD", length = 25)
    private String password;
    @Column(name = "C_ADMIN")
    private boolean admin = false;

    /* ~ ---- Relationships ---- */
    @OneToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "T_USER_CONFIG_JOIN", joinColumns = @JoinColumn(name = "USER_NAME"), inverseJoinColumns = @JoinColumn(name = "CONFIG_ID"))
    private Set<Configuration> parameter = new HashSet<Configuration>();

    /**
     * Preserve a default constructor for the JPA provider.
     */
    protected User() {}

    /**
     * Create a new User.
     * 
     * @param name
     */
    public User(String name) {
        this.name = name;
    }

    /**
     * Get the name.
     * 
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the password.
     * 
     * @return the password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the password.
     * 
     * @param password
     *            The password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get the admin.
     * 
     * @return the admin.
     */
    public boolean isAdmin() {
        return admin;
    }

    /**
     * Set the admin.
     * 
     * @param admin
     *            The admin to set.
     */
    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    /**
     * Get the parameter.
     * 
     * @return the parameter.
     */
    public Set<Configuration> getParameter() {
        return parameter;
    }

    /**
     * Set the parameter.
     * 
     * @param parameter
     *            The parameter to set.
     */
    public void setParameter(Set<Configuration> parameter) {
        this.parameter = parameter;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        User other = (User) obj;
        if (name == null) {
            if (other.name != null) return false;
        } else if (!name.equals(other.name)) return false;
        return true;
    }
}
