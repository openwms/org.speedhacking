/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.common.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * A WorkReport.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
@Entity
@Table(name = "T_WORK_REPORT")
public class WorkReport extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = -2315972106461662814L;
    @ManyToOne
    @JoinColumn(name = "C_RECORD", referencedColumnName = "C_ID")
    private WorkRecord record;
    @Column(name = "C_COMMENT")
    private String comment;
    @Column(name = "C_PROJECT_CODE")
    private String projectCode;

    /**
     * Preserve a default constructor for the JPA provider.
     */
    protected WorkReport() {}

    /**
     * Create a new WorkReport.
     * 
     * @param comment
     * @param projectCode
     */
    public WorkReport(Long id, String comment, String projectCode) {
        super();
        super.setId(id);
        this.comment = comment;
        this.projectCode = projectCode;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((projectCode == null) ? 0 : projectCode.hashCode());
        result = prime * result + ((record == null) ? 0 : record.hashCode());
        return result;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        WorkReport other = (WorkReport) obj;
        if (comment == null) {
            if (other.comment != null) {
                return false;
            }
        } else if (!comment.equals(other.comment)) {
            return false;
        }
        if (projectCode == null) {
            if (other.projectCode != null) {
                return false;
            }
        } else if (!projectCode.equals(other.projectCode)) {
            return false;
        }
        if (record == null) {
            if (other.record != null) {
                return false;
            }
        } else if (!record.equals(other.record)) {
            return false;
        }
        return true;
    }
}
