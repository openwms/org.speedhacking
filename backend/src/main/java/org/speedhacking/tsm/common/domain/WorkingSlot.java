/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.common.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * A WorkingSlot.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
@Entity
@Table(name = "T_WORKING_SLOT")
public class WorkingSlot extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 6114466352544183560L;
    @ManyToOne
    @JoinColumn(name = "C_RECORD", referencedColumnName = "C_ID")
    private WorkRecord record;
    @Column(name = "C_ARRIVED")
    private String arrived;
    @Column(name = "C_LEFT")
    private String left;

    /**
     * Preserve a default constructor for the JPA provider.
     */
    protected WorkingSlot() {}

    /**
     * Create a new WorkingSlot.
     * 
     * @param arrived
     * @param left
     */
    public WorkingSlot(String arrived, String left) {
        super();
        this.arrived = arrived;
        this.left = left;
    }

    /**
     * Create a new WorkingSlot.
     * 
     * @param arrived
     * @param left
     */
    public WorkingSlot(Long id, String arrived, String left) {
        super();
        super.setId(id);
        this.arrived = arrived;
        this.left = left;
    }

    /**
     * Get the arrived.
     * 
     * @return the arrived.
     */
    public String getArrived() {
        return arrived;
    }

    /**
     * Set the arrived.
     * 
     * @param arrived
     *            The arrived to set.
     */
    public void setArrived(String arrived) {
        this.arrived = arrived;
    }

    /**
     * Get the left.
     * 
     * @return the left.
     */
    public String getLeft() {
        return left;
    }

    /**
     * Set the left.
     * 
     * @param left
     *            The left to set.
     */
    public void setLeft(String left) {
        this.left = left;
    }
}
