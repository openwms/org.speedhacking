/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.common.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.joda.time.Period;
import org.speedhacking.tsm.common.WorkRecordConverter;

/**
 * A WorkRecord.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
@Entity
@NamedQueries(value = {
        @NamedQuery(name = WorkRecord.NQ_BY_USER_IN_RANGE, query = "select wr from WorkRecord wr where wr.user = :"
                + WorkRecord.NQ_PARAM_USER + " and wr.date between :" + WorkRecord.NQ_PARAM_STARTDATE + " and :"
                + WorkRecord.NQ_PARAM_ENDDATE + " order by wr.date"),
        @NamedQuery(name = WorkRecord.NQ_BY_USER_AND_DATE, query = "select wr from WorkRecord wr where wr.user = :"
                + WorkRecord.NQ_PARAM_USER + " and wr.date = :" + WorkRecord.NQ_PARAM_DATE) })
@Table(name = "T_WORK_RECORD", uniqueConstraints = { @UniqueConstraint(columnNames = { "C_DATE", "C_USER" }) })
public class WorkRecord extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = -954803360896377631L;
    /**
     * Named Query to find all WorkRecords for a particular User named
     * {@code user} and within a given range. Ordered by date.
     * <ul>
     * <li>{@value #NQ_PARAM_USER}: The User to search for</li>
     * <li>{@value #NQ_PARAM_STARTDATE}: From when to start the search</li>
     * <li>{@value #NQ_PARAM_ENDDATE}: The end date to search for</li>
     * </ul>
     */
    public static final String NQ_BY_USER_IN_RANGE = "WorkRecord.findForUserInRange";
    public static final String NQ_BY_USER_AND_DATE = "WorkRecord.findForUserAndDate";

    public static final String NQ_PARAM_USER = "user";
    public static final String NQ_PARAM_DATE = "date";
    public static final String NQ_PARAM_STARTDATE = "startDate";
    public static final String NQ_PARAM_ENDDATE = "endDate";

    @NotNull
    @ManyToOne
    @JoinColumn(name = "C_USER", referencedColumnName = "C_NAME")
    private User user;
    @Column(name = "C_DATE")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "C_OVERTIME")
    private String overtime;
    @Transient
    private Period overtimeTransient;
    @Column(name = "C_OVERTIME_WEEK")
    private String overtimeCurrentWeek;
    @Transient
    private Period overtimeCurrentWeekTransient;
    @Column(name = "C_OVERTIME_MONTH")
    private String overtimeCurrentMonth;
    @Transient
    private Period overtimeCurrentMonthTransient;
    @Enumerated(EnumType.STRING)
    @Column(name = "C_TYPE")
    private WorkRecordType type;

    /* ~ ---- Relationships ---- */
    @OneToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "T_WR_WS_JOIN", joinColumns = @JoinColumn(name = "WR_ID"), inverseJoinColumns = @JoinColumn(name = "WS_ID"))
    private Set<WorkingSlot> slots = new HashSet<WorkingSlot>();
    @OneToMany(cascade = { CascadeType.ALL })
    @JoinTable(name = "T_WR_REPORT_JOIN", joinColumns = @JoinColumn(name = "WR_ID"), inverseJoinColumns = @JoinColumn(name = "REPORT_ID"))
    private Set<WorkReport> reports = new HashSet<WorkReport>();

    /**
     * Preserve a default constructor for the JPA provider.
     */
    protected WorkRecord() {}

    /**
     * Create a new WorkRecord.
     * 
     * @param user
     * @param date
     */
    public WorkRecord(User user, Date date) {
        this.user = user;
        this.date = date;
    }

    /**
     * Do some transformation after loaded from the db.
     */
    @PostLoad
    protected void postLoad() {
        if (overtime != null && !overtime.isEmpty()) {
            overtimeTransient = WorkRecordConverter.PERIOD_FORMATTER.parsePeriod(overtime);
        }
        if (overtimeCurrentWeek != null && !overtimeCurrentWeek.isEmpty()) {
            overtimeCurrentWeekTransient = WorkRecordConverter.PERIOD_FORMATTER.parsePeriod(overtimeCurrentWeek);
        }
        if (overtimeCurrentMonth != null && !overtimeCurrentMonth.isEmpty()) {
            overtimeCurrentMonthTransient = WorkRecordConverter.PERIOD_FORMATTER.parsePeriod(overtimeCurrentMonth);
        }
    }

    /**
     * Do some transformation before updating or persisting.
     */
    @PrePersist
    @PreUpdate
    protected void preUpdate() {
        if (overtimeTransient != null) {
            overtime = overtimeTransient.toString();
        }
        if (overtimeCurrentWeekTransient != null) {
            overtimeCurrentWeek = overtimeCurrentWeekTransient.toString();
        }
        if (overtimeCurrentMonthTransient != null) {
            overtimeCurrentMonth = overtimeCurrentMonthTransient.toString();
        }
    }

    /**
     * Get the user.
     * 
     * @return the user.
     */
    public User getUser() {
        return user;
    }

    /**
     * Get the date.
     * 
     * @return the date.
     */
    public Date getDate() {
        return date;
    }

    /**
     * Get the overtime.
     * 
     * @return the overtime.
     */
    public Period getOvertime() {
        return overtimeTransient;
    }

    /**
     * Set the overtime.
     * 
     * @param overtime
     *            The overtime to set.
     */
    public void setOvertime(Period overtime) {
        this.overtimeTransient = overtime;
    }

    /**
     * Get the overtimeCurrentWeek.
     * 
     * @return the overtimeCurrentWeek.
     */
    public Period getOvertimeCurrentWeek() {
        return overtimeCurrentWeekTransient;
    }

    /**
     * Get the overtimeCurrentMonth.
     * 
     * @return the overtimeCurrentMonth.
     */
    public Period getOvertimeCurrentMonth() {
        return overtimeCurrentMonthTransient;
    }

    /**
     * Get the type.
     * 
     * @return the type.
     */
    public WorkRecordType getType() {
        return type;
    }

    /**
     * Set the type.
     * 
     * @param type
     *            The type to set.
     */
    public void setType(WorkRecordType type) {
        this.type = type;
    }

    /**
     * Get the slots.
     * 
     * @return the slots.
     */
    public Set<WorkingSlot> getSlots() {
        return slots;
    }

    /**
     * Set the slots.
     * 
     * @param slots
     *            The slots to set.
     */
    public void setSlots(Set<WorkingSlot> slots) {
        this.slots = slots;
    }

    /**
     * FIXME [scherrer] Comment this
     * 
     * @param slot
     * @return
     */
    public boolean addSlot(WorkingSlot slot) {
        return slots.add(slot);
    }

    /**
     * Get the reports.
     * 
     * @return the reports.
     */
    public Set<WorkReport> getReports() {
        return reports;
    }

    /**
     * FIXME [scherrer] Comment this
     * 
     * @param report
     * @return
     */
    public boolean addReport(WorkReport report) {
        return reports.add(report);
    }

    /**
     * Set the reports.
     * 
     * @param reports
     *            The reports to set.
     */
    public void setReports(Set<WorkReport> reports) {
        this.reports = reports;
    }
}
