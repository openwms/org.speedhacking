/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.tsm.common.domain;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 * A Configuration.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
@Entity
@NamedQueries({
        @NamedQuery(name = Configuration.NQ_BY_USER, query = "select c from Configuration c where c.user = :"
                + Configuration.NQ_PARAM_USER),
        @NamedQuery(name = Configuration.NQ_BY_USER_AND_KEY, query = "select c from Configuration c where c.user = :"
                + Configuration.NQ_PARAM_USER + " and c.key = :" + Configuration.NQ_PARAM_KEY) })
@Table(name = "T_CONFIGURATION", uniqueConstraints = @UniqueConstraint(columnNames = { "C_KEY" }))
public class Configuration extends AbstractEntity implements Serializable {

    /**
     * Named Query to find all Configuration objects of an {@code user}.
     * <ul>
     * <li>{@value #NQ_PARAM_NAME}: The User</li>
     * </ul>
     */
    public static final String NQ_BY_USER = "Configuration.findByUser";
    /**
     * Named Query to find a Configuration object of an {@code user} by the
     * {@code key} .
     * <ul>
     * <li>{@value #NQ_PARAM_NAME}: The User</li>
     * <li>{@value #NQ_PARAM_KEY}: The key</li>
     * </ul>
     */
    public static final String NQ_BY_USER_AND_KEY = "Configuration.findByUserAndKey";
    /**
     * Parameter name {@code user}.
     */
    public static final String NQ_PARAM_USER = "user";
    /**
     * Parameter name {@code key}.
     */
    public static final String NQ_PARAM_KEY = "key";

    private static final long serialVersionUID = 1L;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "C_USER", referencedColumnName = "C_NAME")
    private User user;
    @NotNull
    @Column(name = "C_KEY", length = 1000)
    private String key;
    @Column(name = "C_DESC", length = 1000)
    private String description;
    @Column(name = "C_ENABLED")
    private boolean enabled = true;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "C_VALUE")
    private Serializable value;
    @NotNull
    @Column(name = "C_TYPE", length = 1000)
    private String type;
    @Column(name = "C_FORMAT", length = 40)
    private String format;

    /**
     * Preserve a default constructor for the JPA provider.
     */
    protected Configuration() {}

    /**
     * Get the user.
     * 
     * @return the user.
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the user.
     * 
     * @param user
     *            The user to set.
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Get the key.
     * 
     * @return the key.
     */
    public String getKey() {
        return key;
    }

    /**
     * Set the key.
     * 
     * @param key
     *            The key to set.
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Get the description.
     * 
     * @return the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description.
     * 
     * @param description
     *            The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the enabled.
     * 
     * @return the enabled.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Set the enabled.
     * 
     * @param enabled
     *            The enabled to set.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Get the value.
     * 
     * @return the value.
     */
    public Serializable getValue() {
        return value;
    }
    
    /**
     * Set the value.
     * 
     * @param value
     *            The value to set.
     */
    public void setValue(Serializable value) {
        this.value = value;
    }

    /**
     * Get the type.
     * 
     * @return the type.
     */
    public String getType() {
        return type;
    }

    /**
     * Set the type.
     * 
     * @param type
     *            The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get the format.
     * 
     * @return the format.
     */
    public String getFormat() {
        return format;
    }

    /**
     * Set the format.
     * 
     * @param format
     *            The format to set.
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((user == null) ? 0 : user.hashCode());
        return result;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Configuration other = (Configuration) obj;
        if (key == null) {
            if (other.key != null) return false;
        } else if (!key.equals(other.key)) return false;
        if (user == null) {
            if (other.user != null) return false;
        } else if (!user.equals(other.user)) return false;
        return true;
    }
}
