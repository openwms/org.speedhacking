/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.ui;

import android.app.ProgressDialog;

import com.WazaBe.HoloEverywhere.sherlock.SActivity;

/**
 * An AbstractAsyncActivity is used as super class that provides some used
 * components like progressBars aso.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public abstract class AbstractAsyncActivity<T> extends SActivity implements OnSuccessHandler<T> {

    private ProgressDialog progressDialog;
    private boolean destroyed = false;
    protected static final String TAG = AbstractAsyncActivity.class.getSimpleName();

    /**
     * Subclasses have to return themselve as {@link OnSuccessHandler}.
     * 
     * @return The subclass
     */
    protected OnSuccessHandler<T> getCallbackHandler() {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSuccess(T result) {}

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroyed = true;
    }

    /**
     * Show progress dialog.
     */
    public void showLoadingProgressDialog() {
        this.showProgressDialog("Loading. Please wait...");
    }

    /**
     * Show progress dialog with a message text.
     * 
     * @param message
     *            Text to display
     */
    public void showProgressDialog(CharSequence message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
        }

        progressDialog.setMessage(message);
        progressDialog.show();
    }

    /**
     * Remove progress dialog.
     */
    public void dismissProgressDialog() {
        if (progressDialog != null && !destroyed) {
            progressDialog.dismiss();
        }
    }
}
