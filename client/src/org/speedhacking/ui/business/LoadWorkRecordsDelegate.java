/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.ui.business;

import org.speedhacking.tsm.common.WorkRecordResponse;
import org.speedhacking.ui.OnSuccessHandler;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * A LoadWorkRecordsDelegate tries to login to the target backend system and
 * loads a list of WorkRecords. The amount of loaded WorkRecords depends on the
 * url parameter that is passed as thread parameter.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public class LoadWorkRecordsDelegate extends SecureAsyncTask<String, Void, WorkRecordResponse> {

    private OnSuccessHandler<WorkRecordResponse> callbackHandler;

    /**
     * Create a new LoadWorkRecordsDelegate.
     * 
     * @param callbackHandler
     *            The UI class to notify
     */
    public LoadWorkRecordsDelegate(OnSuccessHandler<WorkRecordResponse> callbackHandler) {
        this.callbackHandler = callbackHandler;
    }

    /**
     * {@inheritDoc}
     * 
     * Call the backend service to fetch a list of WorkRecords. The first
     * parameter is expected to be the complete rest url including start and end
     * date.
     */
    @Override
    protected WorkRecordResponse doInBackground(String... params) {
        if (params == null || params.length == 0) {
            throw new IllegalArgumentException("Must at least have the url set as parameter");
        }
        try {
            ResponseEntity<WorkRecordResponse> response = getRestTemplate().exchange(params[0], HttpMethod.GET,
                    new HttpEntity<Object>(getHttpHeader()), WorkRecordResponse.class);
            return response.getBody();
        } catch (Exception rce) {
            WorkRecordResponse res = new WorkRecordResponse();
            res.setError(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return res;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPostExecute(WorkRecordResponse result) {
        callbackHandler.onSuccess(result);
    }
}
