/*
 * openwms.org, the Open Warehouse Management System.
 *
 * This file is part of openwms.org.
 *
 * openwms.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * openwms.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software. If not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.speedhacking.ui.business;

import java.util.Collections;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.os.AsyncTask;

/**
 * A SecureAsyncTask extends the default AsyncTask about JSON, OAuth and REST
 * support.
 * 
 * @author <a href="mailto:scherrer@openwms.org">Heiko Scherrer</a>
 * @version $Revision: $
 * @since 0.1
 */
public abstract class SecureAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    private String username;
    private char[] password;
    private RestTemplate restTemplate;
    private HttpHeaders httpHeader;
    protected static final String TAG = SecureAsyncTask.class.getSimpleName();

    /**
     * Create a new SecureAsyncTask.
     */
    protected SecureAsyncTask() {}

    /**
     * {@inheritDoc}
     * 
     * Before executing any command setup the HttpHeader and authentication.
     * 
     * @see android.os.AsyncTask#onPreExecute()
     */
    @Override
    protected void onPreExecute() {
        // HttpAuthentication authHeader = new HttpBasicAuthentication(username,
        // password.toString());
        httpHeader = new HttpHeaders();
        // httpHeader.setAuthorization(authHeader);
        // httpHeader.setAcceptEncoding(ContentCodingType.GZIP);
        httpHeader.setContentType(MediaType.APPLICATION_JSON);
        httpHeader.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setReadTimeout(10000);
        restTemplate = new RestTemplate(factory);
        restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
    }

    /**
     * Return a prepared template object for RESTful invocations.
     * 
     * @return The RestTemplate
     */
    protected RestTemplate getRestTemplate() {
        return restTemplate;
    }

    /**
     * Return an already prepared HttpHeaders object that can be modified before
     * any remote invocation occurs.
     * 
     * @return The prepared HttpHeader object
     */
    protected HttpHeaders getHttpHeader() {
        return httpHeader;
    }
}
