package org.speedhacking.ui.fragments;


import java.util.Date;

import org.speedhacking.R;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.WazaBe.HoloEverywhere.LayoutInflater;
import com.WazaBe.HoloEverywhere.app.Fragment;
import com.WazaBe.HoloEverywhere.widget.Button;
import com.WazaBe.HoloEverywhere.widget.EditText;
import com.WazaBe.HoloEverywhere.widget.TextView;

public class TodayFragment extends Fragment implements OnClickListener {
	
	private Button saveBtn;
	
	private EditText vonInput;
	
	private EditText bisInput;
	
	private EditText pauseInput;
	
	private TextView heuteAusgabe;
	
	private TextView wocheAusgabe;
	
	private TextView monatAusgabe;
	
	@Override
	public android.view.View onCreateView(LayoutInflater inflater,
			android.view.ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.today);
		this.saveBtn = (Button) view.findViewById(R.id.btnSave);
		this.vonInput = (EditText) view.findViewById(R.id.idVonLabel);
		this.bisInput = (EditText) view.findViewById(R.id.idBisLabel);
		this.pauseInput = (EditText) view.findViewById(R.id.idPauseLabel);
		this.heuteAusgabe = (TextView) view.findViewById(R.id.idAusgabeHeute);
		this.wocheAusgabe = (TextView) view.findViewById(R.id.idAusgabeWoche);
		this.monatAusgabe = (TextView) view.findViewById(R.id.idAusgabeMonat);
		
		this.saveBtn.setOnClickListener(this);
		
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
	}

	@Override
	public void onClick(View view) {
		if (view != saveBtn) {
			throw new IllegalStateException("wtf");
		}
		Date von = parseHHMM(this.vonInput.getText().toString());
		Date bis = parseHHMM(this.bisInput.getText().toString());
		String pauseStr = this.pauseInput.getText().toString();
		
		this.heuteAusgabe.setText("Heute: " + new Date(bis.getSeconds() - von.getSeconds()));
	}
	
	private static Date parseHHMM(String input) {
		String[] parts = input.split(":");
		int hrs = Integer.parseInt(parts[0].trim());
		int min = Integer.parseInt(parts[1].trim());
		return new Date(2012, 11, 16, hrs, min);
	}

}
