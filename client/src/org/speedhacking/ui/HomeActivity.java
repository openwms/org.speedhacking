package org.speedhacking.ui;

import java.util.Collection;
import java.util.Collections;

import org.speedhacking.R;
import org.speedhacking.adapter.HomePagerAdapter;
import org.speedhacking.tsm.common.WorkRecordRequest;
import org.speedhacking.tsm.common.WorkRecordResponse;
import org.speedhacking.tsm.common.WorkRecordWSTO;
import org.speedhacking.tsm.common.WorkingSlotWSTO;
import org.speedhacking.ui.business.AddWorkRecordDelegate;
import org.speedhacking.ui.business.LoadWorkRecordsDelegate;
import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.WazaBe.HoloEverywhere.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.viewpagerindicator.TabPageIndicator;

public class HomeActivity extends AbstractAsyncActivity<WorkRecordResponse> implements ActionBar.OnNavigationListener {

    private ArrayAdapter<CharSequence> navigationList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);

        final Button submitButton = (Button) findViewById(R.id.submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LoadWorkRecordsDelegate(getCallbackHandler()).execute(new String[] { getString(R.string.base_uri)
                        + "/workRecords/2000/01" });
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        navigationList = ArrayAdapter.createFromResource(getSupportActionBar().getThemedContext(), R.array.navigation,
                R.layout.sherlock_spinner_item);
        if (itemId == navigationList.getItemId(0)) {
            Toast.makeText(this, "Menu Item Day selected", Toast.LENGTH_SHORT).show();
        }
        if (itemId == navigationList.getItemId(1)) {
            Toast.makeText(this, "Menu item Week selected", Toast.LENGTH_SHORT).show();
        }
        if (itemId == navigationList.getItemId(2)) {
            Toast.makeText(this, "Menu item Month selected", Toast.LENGTH_SHORT).show();
            setContentView(R.layout.month_view);
        } else {
            Toast.makeText(this, navigationList.getItem(itemPosition), Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    private void createMainLayout() {
        setContentView(R.layout.container);

        Context context = getSupportActionBar().getThemedContext();
        navigationList = ArrayAdapter.createFromResource(context, R.array.navigation, R.layout.sherlock_spinner_item);
        navigationList.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        getSupportActionBar().setListNavigationCallbacks(navigationList, this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        HomePagerAdapter adapter = new HomePagerAdapter(getSupportFragmentManager(), getResources().getStringArray(
                R.array.tabs_home));
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        TabPageIndicator indicator = (TabPageIndicator) findViewById(R.id.indicator);
        pager.setAdapter(adapter);
        indicator.setViewPager(pager);
        indicator.setCurrentItem(HomePagerAdapter.TODAY);
    }

    private void createMonthlyView() {
        setContentView(R.layout.container);

        Context context = getSupportActionBar().getThemedContext();
        navigationList = ArrayAdapter.createFromResource(context, R.array.navigation, R.layout.sherlock_spinner_item);
        navigationList.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        getSupportActionBar().setListNavigationCallbacks(navigationList, this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        HomePagerAdapter adapter = new HomePagerAdapter(getSupportFragmentManager(), getResources().getStringArray(
                R.array.tabs_home));
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        TabPageIndicator indicator = (TabPageIndicator) findViewById(R.id.indicator);
        pager.setAdapter(adapter);
        indicator.setViewPager(pager);
        indicator.setCurrentItem(HomePagerAdapter.TODAY);
    }

    /**
     * @see org.speedhacking.ui.OnSuccessHandler#onSuccess(java.lang.Object)
     */
    @Override
    public void onSuccess(WorkRecordResponse result) {
        if (result.getError() == HttpStatus.OK.value()) {

            Collection<WorkRecordWSTO> requests = result.getSaved();
            if (requests.size() > 0) {
                WorkRecordWSTO request = requests.iterator().next();
                request.addSlot(new WorkingSlotWSTO("09:00", "11:30"));
                request.addSlot(new WorkingSlotWSTO("12:30", "19:00"));

                WorkRecordRequest workRecordRequest = new WorkRecordRequest();
                workRecordRequest.setSlots(request.getSlots());
                workRecordRequest.setReports(request.getReports());
                workRecordRequest.setDate(request.getDate());
                workRecordRequest.setId(request.getId());
                workRecordRequest.setType("");

                new AddWorkRecordDelegate(getCallbackHandler()).execute(new Object[] {
                        getString(R.string.base_uri) + "/workRecords", workRecordRequest });

            }
            dismissProgressDialog();
            displayResponse(result);
        } else {
            Toast.makeText(this, getApplicationContext().getString(R.string.toast_server_error), Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void displayResponse(WorkRecordResponse response) {
        createMainLayout();
        // Toast.makeText(this, response.getError(), Toast.LENGTH_LONG).show();
    }

    @Deprecated
    private class FetchSecuredResourceTask extends AsyncTask<Void, Void, WorkRecordResponse> {

        private String username;

        private String password;

        @Override
        protected void onPreExecute() {
            // showLoadingProgressDialog();

            // build the message object
            EditText editText = (EditText) findViewById(R.id.username);
            this.username = editText.getText().toString();

            editText = (EditText) findViewById(R.id.password);
            this.password = editText.getText().toString();
        }

        @Override
        protected WorkRecordResponse doInBackground(Void... params) {
            final String url = getString(R.string.base_uri) + "/workRecords/2000/01";

            HttpAuthentication authHeader = new HttpBasicAuthentication(username, password);
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setAuthorization(authHeader);
            requestHeaders.setContentType(MediaType.APPLICATION_JSON);
            requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            // Create a new RestTemplate instance
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());

            try {
                Log.d(TAG, url);
                ResponseEntity<WorkRecordResponse> response = restTemplate.exchange(url, HttpMethod.GET,
                        new HttpEntity<Object>(requestHeaders), WorkRecordResponse.class);
                return response.getBody();
            } catch (Exception e) {
                Log.e(TAG, e.getLocalizedMessage(), e);
                return new WorkRecordResponse();
            }
        }

        @Override
        protected void onPostExecute(WorkRecordResponse result) {
            dismissProgressDialog();
            displayResponse(result);
        }
    }
}
