package org.speedhacking.persistence.impl;

import org.speedhacking.persistence.UserService;
import org.speedhacking.persistence.data.user.User;

public class UserServiceImpl implements UserService {

	@Override
	public String authenticateUser(String user, String password) {
		/* API: GET /zeiten/v1/login
		 * JSON Request:
		 * {
		 * 	"user": {
		 * 		"name": <unique identifier>
		 * 		"password": <hashed password>
		 * 	}
		 * }
		 * JSON Response:
		 * {
		 *  "user": {
		 *   	"name": <unique identifier>
		 *   	"token": <token>
		 *  }
		 * }
		 */
		return null;
	}
	
	@Override
	public User loadUser(String token) {
		/* API: GET /zeiten/v1/user
		 * JSON Request:
		 * {
		 * 	"user": {
		 * 		"token": <token>
		 * 	}
		 * }
		 * JSON Response:
		 * {
		 *  "user": {
		 *   	"name": <unique identifier>
		 *   	... TODO ...
		 *  }
		 * }
		 */
		return null;
	}
	
	@Override
	public boolean deleteUser(String token) {
		/* API: DELETE /zeiten/v1/user
		 * JSON Request:
		 * {
		 * 	"user": {
		 * 		"token": <token>
		 * 	}
		 * }
		 * JSON Response:
		 * {
		 *  "success": <true/false>
		 *  }
		 * }
		 */
		return false;
	}

	@Override
	public User updateUserName(String token, String name) {
		/* API: POST /zeiten/v1/user
		 * JSON Request:
		 * {
		 * 	"user": {
		 * 		"token": <token>
		 * 		"name": <new name>
		 * 	}
		 * }
		 * JSON Response:
		 * {
		 *  "user": {
		 *  	"name": <name>
		 *  	... TODO ...
		 *  }
		 * }
		 */
		return null;
	}
}