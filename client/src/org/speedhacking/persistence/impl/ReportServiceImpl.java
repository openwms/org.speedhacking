package org.speedhacking.persistence.impl;

import java.util.List;

import org.speedhacking.persistence.ReportService;
import org.speedhacking.persistence.data.BreakTime;
import org.speedhacking.persistence.data.DailyReport;
import org.speedhacking.persistence.data.MonthlyReport;
import org.speedhacking.persistence.data.WeeklyReport;
import org.speedhacking.persistence.data.WorkingTime;

public class ReportServiceImpl implements ReportService {

	@Override
	public DailyReport reportDay(WorkingTime workingTime, List<BreakTime> breakTimes) {
		/* API: PUT /zeiten/v1/report/day
		 * JSON Request: 
		 * {
		 *  "day": {
		 *   "date": <date>,
		 *   "arrivalTime": <arrival time>,
		 *   "departureTime": <departure time>,
		 *   "breakTimes": [
		 *     {
		 *    	"type": <lunch/coffee/private>,
		 *    	"startingTime": <starting time>,
		 *    	"endingTime": <ending time>
		 *     }
		 *    ]
		 *  }
		 * }
		 * JSON Response:
		 * TODO
		 */
		return null;
	}

	@Override
	public DailyReport reportDay(WorkingTime workingTime,
			List<BreakTime> breakTimes, String comment) {
		// XXX API: PUT /zeiten/v1/report/day
		// XXX JSON: TODO
		return null;
	}

	@Override
	public DailyReport getDay(int calendarDay) {
		/* XXX API: GET /zeiten/v1/report/day/<calendarDay>
		 * XXX JSON: TODO
		 */
		return null;
	}

	@Override
	public WeeklyReport getWeek(int calendarWeek) {
		/* XXX API: GET /zeiten/v1/report/week/<calendarWeek>
		 * XXX JSON: TODO
		 */
		return null;
	}

	@Override
	public MonthlyReport getMonth(int calendarMonth) {
		/* XXX API: GET /zeiten/v1/report/month/<calendarMonth>
		 * XXX JSON: TODO
		 */
		return null;
	}
}