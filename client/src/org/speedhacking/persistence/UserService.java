package org.speedhacking.persistence;

import org.speedhacking.persistence.data.user.User;

public interface UserService {
	/**
	 * Authenticates an user.
	 * @param user The user name (= the unique identifier)
	 * @param password The hashed password
	 * @return A token used for every further action or <code>null</code> if the user-password combination is invalid.
	 */
	String authenticateUser(final String user, final String password);
	
	/**
	 * Loads an user.
	 * @param token The token to be used
	 * @return An object holding all user data
	 */
	User loadUser(final String token);
	
	/**
	 * Updates an user name.
	 * @param token The token to be used
	 * @param name The new user name
	 * @return An object holding all user data
	 */
	User updateUserName(final String token, final String name);
	
	/**
	 * Deletes an user. The <code>token</code> will be invalid if the return value is <code>true</code>.
	 * @param token The token to be used
	 * @return <code>true</code> if the user has been deleted, false otherwise
	 */
	boolean deleteUser(String token);
}
