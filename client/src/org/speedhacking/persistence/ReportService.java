package org.speedhacking.persistence;

import java.util.List;

import org.speedhacking.persistence.data.BreakTime;
import org.speedhacking.persistence.data.DailyReport;
import org.speedhacking.persistence.data.MonthlyReport;
import org.speedhacking.persistence.data.WeeklyReport;
import org.speedhacking.persistence.data.WorkingTime;


public interface ReportService {
	
	DailyReport reportDay(final WorkingTime workingTime, final List<BreakTime> breakTimes);
	
	DailyReport reportDay(final WorkingTime workingTime, final List<BreakTime> breakTimes, final String comment);
	
	
	DailyReport getDay(final int calendarDay);
	
	WeeklyReport getWeek(final int calendarWeek);
	
	MonthlyReport getMonth(final int calendarMonth);
}
