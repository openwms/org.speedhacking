package org.speedhacking.persistence.data;

import java.util.ArrayList;
import java.util.List;

public class MonthlyReport {
	private Integer calendarMonth = null;
	private List<DailyReport> dailyReports = new ArrayList<DailyReport>(31);
	
	public MonthlyReport(Integer calendarMonth, List<DailyReport> dailyReports) {
		this.calendarMonth = calendarMonth;
		this.dailyReports = dailyReports;
	}
	
	public Integer getCalendarMonth() {
		return calendarMonth;
	}
	
	public List<DailyReport> getDailyReports() {
		return dailyReports;
	}
}
