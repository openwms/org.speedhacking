package org.speedhacking.persistence.data;

import java.util.ArrayList;
import java.util.List;

public class WeeklyReport {
	private Integer calendarWeek = null;
	private List<DailyReport> dailyReports = new ArrayList<DailyReport>(7);
	
	public WeeklyReport(Integer calendarWeek, List<DailyReport> dailyReports) {
		this.calendarWeek = calendarWeek;
		this.dailyReports = dailyReports;
	}
	
	public Integer getCalendarWeek() {
		return calendarWeek;
	}
	public List<DailyReport> getDailyReports() {
		return dailyReports;
	}
}
