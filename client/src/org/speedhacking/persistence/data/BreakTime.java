package org.speedhacking.persistence.data;

import java.util.Date;

public final class BreakTime {
	private Date start = null;
	private Date end = null;
	
	private BreakTime.Type type = null;

	public BreakTime(final BreakTime.Type type, final Date start, final Date end) {
		this.type = type;
		this.start = start;
		this.end = end;
	}
	
	public Type getType() {
		return type;
	}

	public Date getStart() {
		return start;
	}

	public Date getEnd() {
		return end;
	}
	
	public enum Type {
		LUNCH_BREAK, COFFEE_BREAK, PRIVATE;
	}
}
