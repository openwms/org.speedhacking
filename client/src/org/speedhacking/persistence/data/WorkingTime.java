package org.speedhacking.persistence.data;

import java.util.Date;

public class WorkingTime {
	private Date workingTimeStart = null;
	private Date workingTimeEnd = null;
	
	public WorkingTime(final Date workingTimeStart, final Date workingTimeEnd) {
		this.workingTimeEnd = workingTimeEnd;
		this.workingTimeStart = workingTimeStart;
	}
	
	public Date getWorkingTimeStart() {
		return workingTimeStart;
	}

	public Date getWorkingTimeEnd() {
		return workingTimeEnd;
	}
}
