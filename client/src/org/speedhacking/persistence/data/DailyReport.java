package org.speedhacking.persistence.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class DailyReport {
	private Integer id = null;
	private Date reportDate = null;
	
	private List<BreakTime> breakTimes = new ArrayList<BreakTime>();
	private BreakTime lunchBreak = null;
	
	private String comment = null;

	
	public Integer getId() {
		return id;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public List<BreakTime> getBreakTimes() {
		return breakTimes;
	}

	public BreakTime getLunchBreak() {
		return lunchBreak;
	}

	public String getComment() {
		return comment;
	}
}