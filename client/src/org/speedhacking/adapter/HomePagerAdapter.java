package org.speedhacking.adapter;

import org.speedhacking.ui.fragments.TodayFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class HomePagerAdapter extends FragmentPagerAdapter {

    public static final int YESTERDAY = 0;
    public static final int TODAY = 1;
    public static final int TOMORROW = 2;
    private static final int COUNT = 3;

    private String[] titles;

    public HomePagerAdapter(FragmentManager fm, String[] titles) {
        super(fm);
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
        case YESTERDAY:
            return new Fragment();
        case TODAY:
            return new TodayFragment();
        case TOMORROW:
            return new Fragment();
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return COUNT;
    }

}
